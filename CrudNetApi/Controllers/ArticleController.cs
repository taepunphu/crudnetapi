using System.Threading.Tasks;
using CrudNetApi.Shared.Facades;
using CrudNetApi.Shared.Models;
using Microsoft.AspNetCore.Mvc;

namespace CrudNetApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly IArticleFacade _facade;

        public ArticleController(IArticleFacade facade)
        {
            _facade = facade;
        }

        [HttpPost]
        public async Task<IActionResult> CreateArticle(ArticleRequest model)
        {
            var resp = await _facade.CreateArticle(model);
            return new JsonResult(resp)
            {
                StatusCode = (resp.Result) ? 201 : 400
            };
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateArticle(int id, ArticleRequest model)
        {
            var resp = await _facade.UpdateArticle(id, model);
            return new JsonResult(resp)
            {
                StatusCode = (resp.Result) ? 200 : 400
            };
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetArticleById(int id)
        {
            var resp = await _facade.GetArticleById(id);
            return Ok(resp);
        }

        [HttpGet]
        public async Task<IActionResult> GetArticles()
        {
            var resp = await _facade.GetArticles();
            return Ok(resp);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteArticle(int id)
        {
            var resp = await _facade.DeleteArticle(id);
            return Ok(resp);
        }
    }
}