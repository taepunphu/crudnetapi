using CrudNetApi.Models;
using CrudNetApi.Shared.Facades;
using CrudNetApi.Shared.Helpers;
using CrudNetApi.Shared.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var appSettings = new AppSettings()
{
    DefaultConnectionString = builder.Configuration.GetSection("DefaultConnectionString").Get<string>()
};

var sqlConnectionFactory = new SqlConnectionFactory(appSettings.DefaultConnectionString);
builder.Services.AddTransient<ISqlConnectionFactory, SqlConnectionFactory>(x => sqlConnectionFactory);

var articleRepository = new ArticleRepository(sqlConnectionFactory);
builder.Services.AddSingleton<IArticleRepository, ArticleRepository>(x => articleRepository);

var articleFacade = new ArticleFacade(articleRepository);
builder.Services.AddScoped<IArticleFacade, ArticleFacade>(x => articleFacade);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
