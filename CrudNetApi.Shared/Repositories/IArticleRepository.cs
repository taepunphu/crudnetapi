using System.Collections.Generic;
using System.Threading.Tasks;
using CrudNetApi.Shared.Models;

namespace CrudNetApi.Shared.Repositories
{
    public interface IArticleRepository
    {
        Task<IEnumerable<Article>> GetArticles();
        Task<Article> GetArticleById(int id);
        Task<int> CreateArticle(ArticleRequest articleRequest);
        Task<int> UpdateArticle(int id, ArticleRequest articleRequest);
        Task<int> DeleteArticle(int id);
    }
}