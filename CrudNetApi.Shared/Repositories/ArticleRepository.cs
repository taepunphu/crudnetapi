using System.Collections.Generic;
using System.Threading.Tasks;
using CrudNetApi.Shared.Helpers;
using CrudNetApi.Shared.Models;
using Dapper;

namespace CrudNetApi.Shared.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public ArticleRepository(ISqlConnectionFactory sqlConnectionFactory)
        {
            _sqlConnectionFactory = sqlConnectionFactory;
        }

        public async Task<int> CreateArticle(ArticleRequest articleRequest)
        {
            using var sqlConnection = _sqlConnectionFactory.GetOpenConnection();

            string sql = @"INSERT INTO Articles(Title, Content, CreatedAt, UpdatedAt) 
                           OUTPUT INSERTED.Id
                           VALUES(@Title, @Content, GETDATE(), GETDATE()); ";

            return await sqlConnection.ExecuteScalarAsync<int>(sql, articleRequest);
        }

        public async Task<int> DeleteArticle(int id)
        {
            using var sqlConnection = _sqlConnectionFactory.GetOpenConnection();

            string sql = @"DELETE FROM Articles WHERE Id = @Id ";

            return await sqlConnection.ExecuteAsync(sql, new { Id = id });
        }

        public async Task<Article> GetArticleById(int id)
        {
            using var sqlConnection = _sqlConnectionFactory.GetOpenConnection();

            string sql = @"SELECT * FROM Articles WHERE Id = @Id ";

            return await sqlConnection.QueryFirstOrDefaultAsync<Article>(sql, new { Id = id });
        }

        public async Task<IEnumerable<Article>> GetArticles()
        {
            using var sqlConnection = _sqlConnectionFactory.GetOpenConnection();

            string sql = @"SELECT * FROM Articles ";

            return await sqlConnection.QueryAsync<Article>(sql);
        }

        public async Task<int> UpdateArticle(int id, ArticleRequest articleRequest)
        {
            using var sqlConnection = _sqlConnectionFactory.GetOpenConnection();

            string sql = @"UPDATE Articles SET Title = @Title, Content = @Content, UpdatedAt = GETDATE()
                           WHERE Id = @Id ";

            return await sqlConnection.ExecuteAsync(sql, new {Id = id, Title = articleRequest.Title, Content = articleRequest.Content});
        }
    }
}