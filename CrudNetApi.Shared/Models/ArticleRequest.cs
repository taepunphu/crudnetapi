namespace CrudNetApi.Shared.Models
{
    public class ArticleRequest
    {
        public string Title {get; set;}
        public string Content {get; set;}
    }
}