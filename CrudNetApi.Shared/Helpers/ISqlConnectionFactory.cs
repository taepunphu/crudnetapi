using System.Data;

namespace CrudNetApi.Shared.Helpers
{
    public interface ISqlConnectionFactory
    {
        IDbConnection GetOpenConnection();
        void Dispose();
    }
}