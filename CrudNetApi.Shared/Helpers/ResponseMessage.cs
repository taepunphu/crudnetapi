using System.Collections.Generic;

namespace CrudNetApi.Shared.Helpers
{
    public class ResponseMessage<T>
    {
        public string Message { get; set; }
        public int? LastPage { get; set; }
        public T Result { get; set; }
        public IEnumerable<T> Results { get; set; }
    }
}