using System.Threading.Tasks;
using CrudNetApi.Shared.Helpers;
using CrudNetApi.Shared.Models;

namespace CrudNetApi.Shared.Facades
{
    public interface IArticleFacade
    {
        Task<ResponseMessage<Article>> GetArticles();
        Task<ResponseMessage<Article>> GetArticleById(int id);
        Task<ResponseMessage<bool>> CreateArticle(ArticleRequest articleRequest);
        Task<ResponseMessage<bool>> UpdateArticle(int id, ArticleRequest articleRequest);
        Task<ResponseMessage<bool>> DeleteArticle(int id);
    }
}