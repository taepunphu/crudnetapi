using System.Linq;
using System.Threading.Tasks;
using CrudNetApi.Shared.Helpers;
using CrudNetApi.Shared.Models;
using CrudNetApi.Shared.Repositories;

namespace CrudNetApi.Shared.Facades
{
    public class ArticleFacade : IArticleFacade
    {
        private readonly IArticleRepository _repo;

        public ArticleFacade(IArticleRepository repo)
        {
            _repo = repo;
        }

        public async Task<ResponseMessage<bool>> CreateArticle(ArticleRequest articleRequest)
        {
            var resp = new ResponseMessage<bool>
            {
                Message = Message.Fail,
                Result = false
            };

            var id = await _repo.CreateArticle(articleRequest);
            if (id < 0) return resp;

            resp.Message = Message.Success;
            resp.Result = true;

            return resp;
        }

        public async Task<ResponseMessage<bool>> DeleteArticle(int id)
        {
            var resp = new ResponseMessage<bool>
            {
                Message = Message.Fail,
                Result = false
            };

            var result = await _repo.DeleteArticle(id);
            if (result < 0) return resp;

            resp.Message = Message.Success;
            resp.Result = true;
            return resp;
        }

        public async Task<ResponseMessage<Article>> GetArticleById(int id)
        {
            var resp = new ResponseMessage<Article>
            {
                Message = Message.DataNotFound,
            };

            var dataSource = await _repo.GetArticleById(id);
            if (dataSource == null) return resp;

            resp.Message = Message.Success;
            resp.Result = dataSource;
            return resp;
        }

        public async Task<ResponseMessage<Article>> GetArticles()
        {
            var resp = new ResponseMessage<Article>
            {
                Message = Message.DataNotFound,
            };

            var dataSource = await _repo.GetArticles();
            if (!dataSource.Any()) return resp;

            resp.Message = Message.Success;
            resp.Results = dataSource;
            return resp;
        }

        public async Task<ResponseMessage<bool>> UpdateArticle(int id, ArticleRequest articleRequest)
        {
            var resp = new ResponseMessage<bool>
            {
                Message = Message.Fail,
                Result = false
            };

            var dataSource = await _repo.UpdateArticle(id, articleRequest);
            if (dataSource < 0) return resp;

            resp.Message = Message.Success;
            resp.Result = true;

            return resp;
        }
    }
}